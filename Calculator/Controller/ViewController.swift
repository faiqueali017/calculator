//
//  ViewController.swift
//  Calculator
//
//  Created by Faiq on 16/03/2021.
//  Copyright © 2021 London App Brewery. All rights reserved.
//


import UIKit

class ViewController: UIViewController {
    
    //MARK:- IBOutlets
    @IBOutlet weak var displayLabel: UILabel!
    
    //MARK:- Variables
    private var isFinishedTypingNumber: Bool = true
    
    private var displayValue: Double {
        get {
            guard let number = Double(displayLabel.text!) else {
                fatalError("Cannot convert display label text to a Double.")
            }
            return number
        }
        set {
            displayLabel.text = String(newValue)
        }
    }
    
    //instance of Calculator
    private var calculator = CalculatorLogic()
    
    
    //MARK:- IBActions
    @IBAction func calcButtonPressed(_ sender: UIButton) {
        ///What should happen when a non-number button is pressed
        
        isFinishedTypingNumber = true
        
        calculator.setNumber(displayValue)
        
        if let calcMehtod = sender.currentTitle {
            if let result = calculator.calculate(symbol: calcMehtod) {
                displayValue = result
            }
        }
        
    }

    @IBAction func numButtonPressed(_ sender: UIButton) {
        ///What should happen when a number is entered into the keypad
        
        if let numValue = sender.currentTitle {
            if isFinishedTypingNumber {
                displayLabel.text = numValue
                isFinishedTypingNumber = false
            }else{
                if numValue == "." {
                    let isInt = floor(displayValue) == displayValue
                    if !isInt {
                        //to return when user hits another dot(.)
                        return
                    }
                }
                displayLabel.text = displayLabel.text! + numValue
            }
        }
        
    
    }

}

